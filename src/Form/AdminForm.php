<?php

namespace Drupal\entity_term\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity term settings form.
 */
class AdminForm extends ConfigFormBase {

  /**
   * The entity type id.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_term_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_term.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_term.settings');

    // Fieldset for custom module configuration.
    $form['entity_term_sets'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity term sets'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // Prepare the entity selector.
    $entity_type_options = [];

    // Get all applicable entity types.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (is_subclass_of($entity_type->getClass(), FieldableEntityInterface::class) && $entity_type->hasLinkTemplate('canonical')) {
        $entity_type_options[$entity_type_id] = $entity_type->getLabel();
      }
    }

    $form['entity_term_sets']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity'),
      '#options' => $entity_type_options,
      '#empty_option' => $this->t('- Select -'),
      '#limit_validation_errors' => [['entity_term_sets', 'entity_type']],
      '#submit' => ['::submitSelectEntityType'],
      '#executes_submit_callback' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxReplaceEntityTermSetForm',
        'wrapper' => 'entity-term-bundle',
        'method' => 'replace',
      ],
    ];

    $form['entity_term_sets']['bundle_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="entity-term-bundle">',
      '#suffix' => '</div>',
    ];

    if ($this->entityTypeId) {

      $bundles = $this->entityTypeBundleInfo->getBundleInfo($this->entityTypeId);
      $bundle_options = [];

      foreach ($bundles as $id => $info) {
        $bundle_options[$id] = $info['label'];
      }

      $form['entity_term_sets']['bundle_container']['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#options' => $bundle_options,
        '#empty_option' => $this->t('- Select -'),
      ];

    }

    $vocabularies = Vocabulary::loadMultiple();
    $vocabulary_options = [];

    foreach ($vocabularies as $vid => $vocabulary) {
      $vocabulary_options[$vid] = $vocabulary->label();
    }

    $form['entity_term_sets']['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $vocabulary_options,
      '#empty_option' => $this->t('- Select -'),
    ];

    $form['entity_term_sets']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#validate' => ['::validateAdd'],
      '#submit' => ['::submitAdd'],
      '#button_type' => 'primary',
      '#suffix' => '<p>&nbsp;</p>',
    ];

    // Show and change all custom configurations.
    $form['entity_term_sets']['active'] = [
      '#type' => 'table',
      '#header' => [
        'entity' => $this->t('Entity type'),
        'bundle' => $this->t('Bundle'),
        'vocabulary' => $this->t('Vocabulary'),
        'status' => $this->t('Status'),
        'remove' => $this->t('Remove'),
      ],
      '#empty' => $this->t('No configuration added yet.'),
    ];

    if ($entity_term_sets = $config->get('entity_term_sets')) {
      foreach ($entity_term_sets as $key => $settings) {

        $row = [
          'entity_type' => ['#markup' => $settings['entity_type']],
          'bundle' => ['#markup' => $settings['bundle']],
          'vocabulary' => ['#markup' => $settings['vocabulary']],
          'status' => ['#markup' => (!empty($settings['status']) ? $this->t('synced') : $this->t('new'))],
        ];

        $row['remove'] = [
          '#type' => 'checkbox',
          '#default_value' => $key,
        ];

        $form['entity_term_sets']['active'][$key] = $row;
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitAdd(array &$form, FormStateInterface $form_state) {

    $settings = [
      'entity_type' => $form_state->getValue(['entity_term_sets', 'entity_type']),
      'bundle' => $form_state->getValue([
        'entity_term_sets',
        'bundle_container',
        'bundle',
      ]),
      'vocabulary' => $form_state->getValue([
        'entity_term_sets',
        'vocabulary',
      ]),
    ];

    $config = $this->config('entity_term.settings');
    $entity_term_sets = !empty($config->get('entity_term_sets')) ? $config->get('entity_term_sets') : [];

    $key = implode('-', $settings);

    $entity_term_sets[$key] = $settings;

    $config->set('entity_term_sets', $entity_term_sets);
    $config->save();

    $this->messenger()->addMessage($this->t('The configuration has been added.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_term.settings');
    $entity_term_sets = !empty($config->get('entity_term_sets')) ? $config->get('entity_term_sets') : [];

    $updates = [];

    if ($form_state->hasValue(['entity_term_sets', 'active']) && is_array($form_state->getValue(['entity_term_sets', 'active']))) {
      foreach ($form_state->getValue(['entity_term_sets', 'active']) as $key => $values) {
        if (!empty($values['remove']) || empty($entity_term_sets[$key]['status'])) {
          if (!empty($values['remove'])) {
            $updates += $this->getEntityTerms($entity_term_sets[$key], 'remove');
            // Remove the config.
            unset($entity_term_sets[$key]);
          }
          else {
            $updates += $this->getEntityTerms($entity_term_sets[$key], 'add');
            $entity_term_sets[$key]['status'] = TRUE;
          }
        }
      }

      // Prepare the batch.
      $batch = [
        'title' => $this->t('Processing terms.'),
        'operations' => [],
        'init_message' => $this->t('Starting term processing.'),
        'progress_message' => $this->t('Processed @current out of @total.'),
        'error_message' => $this->t('An error occurred during processing.'),
      ];

      foreach ($updates as $update) {
        $batch['operations'][] = ['\Drupal\entity_term\Form\AdminForm::batchProcess', [$update]];
      }

      batch_set($batch);
    }

    $config->set('entity_term_sets', $entity_term_sets);

    // Finally save the configuration.
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Handles submit call when entity type is selected.
   */
  public function submitSelectEntityType(array $form, FormStateInterface $form_state) {
    $this->entityTypeId = $form_state->getValue(['entity_term_sets', 'entity_type']);
    $form_state->setRebuild();
  }

  /**
   * Handles switching the type selector.
   */
  public function ajaxReplaceEntityTermSetForm($form, FormStateInterface $form_state) {
    return $form['entity_term_sets']['bundle_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateAdd(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue(['entity_term_sets', 'entity_type']) == '') {
      $form_state->setErrorByName('entity_term_sets][entity_type', $this->t('You must select an entity type.'));
    }
    else {
      $fields = ['entity_term_sets', 'bundle_container', 'bundle'];
      if ($form_state->getValue($fields) == '') {
        $form_state->setErrorByName('entity_term_sets][bundle_container][bundle', $this->t('You must select a bundle.'));
      }
    }
    if ($form_state->getValue(['entity_term_sets', 'vocabulary']) == '') {
      $form_state->setErrorByName('entity_term_sets][vocabulary', $this->t('You must select a vocabulary.'));
    }
  }

  /**
   * Term add/removal.
   */
  public static function batchProcess($update, &$context) {

    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    $term_properties = [
      'vid' => $update['vocabulary'],
      'name' => $update['label'],
    ];

    // Check for existing term.
    $terms = $term_storage->loadByProperties($term_properties);

    if ($update['operation'] === 'remove' && !empty($terms)) {
      $term_storage->delete($terms);
    }
    elseif (empty($terms)) {
      $term = $term_storage->create($term_properties);
      $term->save();
    }

  }

  /**
   * Provides a list of terms to add or remove.
   */
  public function getEntityTerms($entity_term_set, $operation) {
    $terms = [];

    // Get the entity type info so we can identify the terms to add/remove.
    $entity_type = $this->entityTypeManager->getDefinition($entity_term_set['entity_type']);
    $bundle_key = $entity_type->getKey('bundle');

    // Load all the entities by bundle.
    $entities = $this->entityTypeManager->getStorage($entity_term_set['entity_type'])
      ->loadByProperties([$bundle_key => $entity_term_set['bundle']]);

    if ($entities) {
      foreach ($entities as $entity) {
        // Add the vocabulary and entity label to the return array.
        $terms[] = [
          'operation' => $operation,
          'vocabulary' => $entity_term_set['vocabulary'],
          'label' => $entity->label(),
        ];
      }
    }

    return $terms;
  }

}
