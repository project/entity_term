<?php

namespace Drupal\entity_term\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides the TermViewSubscriber class.
 */
class TermViewSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TermViewSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory->get('entity_term.settings');
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function onRespond(ResponseEvent $event) {

    // Get the entity term sets from config.
    $entity_term_sets = $this->configFactory->get('entity_term_sets');

    // Bail out early if there are no term sets.
    if (!$entity_term_sets) {
      return;
    }

    // Get the request from the event.
    $request = $event->getRequest();

    $route_name = $request->attributes->get('_route');

    // Bail out for any requests other than a term's canonical.
    if ($route_name !== 'entity.taxonomy_term.canonical') {
      return;
    }

    // Bail out if the request is not for a term.
    $term = $request->attributes->get('taxonomy_term');
    if (!$term instanceof Term) {
      return;
    }

    // Bail out early if the term is not in a configured vocabulary.
    $vocabularies = array_unique(array_column($entity_term_sets, 'vocabulary'));
    if (!in_array($term->bundle(), $vocabularies)) {
      return;
    }

    // Loop over the term sets and compare the bundles.
    foreach ($entity_term_sets as $entity_term_set) {
      if (
        $term->bundle() === $entity_term_set['vocabulary']
      ) {

        // Get the entity type definition so we can build a query.
        $entity_type = $this->entityTypeManager
          ->getDefinition($entity_term_set['entity_type']);

        $label_key = $entity_type->getKey('label');
        $bundle_key = $entity_type->getKey('bundle');

        $query = $this->entityTypeManager->getStorage($entity_term_set['entity_type'])
          ->getQuery();

        // Run the query for the entity having the same label as the term.
        $results = $query
          ->condition($bundle_key, $entity_term_set['bundle'])
          ->condition($label_key, $term->label())
          ->accessCheck(FALSE)
          ->execute();

        if ($results) {
          // Load the entity so we can get its canonical link.
          $entity = $this->entityTypeManager
            ->getStorage($entity_term_set['entity_type'])
            ->load(reset($results));

          $response = new RedirectResponse($entity->toUrl()->toString());
          $response->send();

          break;
        }

      }
    }

  }

}
