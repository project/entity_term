<?php

namespace Drupal\entity_term\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Rewrites links for terms which are synced with nodes.
 */
class TaxonomyPathProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TaxonomyPathProcessor object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory->get('entity_term.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {

    if (!isset($options['entity']) || !$options['entity'] instanceof TermInterface) {
      return $path;
    }

    // Bail on edit links.
    if (str_ends_with($path, '/edit')) {
      return $path;
    }

    // Get the entity term sets from config.
    $entityTermSets = $this->configFactory->get('entity_term_sets');

    if (!$entityTermSets) {
      return $path;
    }

    $term = $options['entity'];

    // Bail out early if the term is not in a configured vocabulary.
    $vocabularies = array_unique(array_column($entityTermSets, 'vocabulary'));
    if (!in_array($term->bundle(), $vocabularies)) {
      return $path;
    }

    // Loop over the term sets and compare the bundles.
    foreach ($entityTermSets as $entityTermSet) {
      if ($term->bundle() === $entityTermSet['vocabulary']) {
        // Get the entity type definition so we can build a query.
        $entityType = $this->entityTypeManager
          ->getDefinition($entityTermSet['entity_type']);

        $labelKey = $entityType->getKey('label');
        $bundleKey = $entityType->getKey('bundle');

        $query = $this->entityTypeManager->getStorage($entityTermSet['entity_type'])
          ->getQuery();

        // Run the query for the entity having the same label as the term.
        $results = $query
          ->condition($bundleKey, $entityTermSet['bundle'])
          ->condition($labelKey, $term->label())
          ->accessCheck(FALSE)
          ->execute();

        if ($results) {
          // Load the entity so we can get its canonical link.
          $entity = $this->entityTypeManager
            ->getStorage($entityTermSet['entity_type'])
            ->load(reset($results));

          if ($bubbleable_metadata) {
            $bubbleable_metadata->addCacheableDependency($term);
          }

          return $entity->toUrl()->toString();
        }
      }
    }

    return $path;
  }

}
