CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The purpose of this module is to create terms in a selected vocabulary matching labels for a selected entity type and bundle. One use-case for this is when you need to allow nodes to be used as tags. For example, say you have a "Person" content type, which contains authors or people of interest and you'd like to tag content such as articles, quotes, books, etc. with those people.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/entity_term

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/entity_term


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No special recommendation.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------
We need to configure by visiting: Home>Administration>Configuration>System>Configure Entity term

The module provides an interface where one or more sets of entities, bundles and vocabularies can be selected. When the configuration is saved, a batch process is ran to programmatically create one new term for each entity from the selected entity type and bundle.


MAINTAINERS
-----------

Current maintainers:
 * Cameron Prince (cameronprince) - https://www.drupal.org/u/cameronprince

